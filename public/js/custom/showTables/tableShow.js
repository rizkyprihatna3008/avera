function showMitraTable(){
    getMitra().then(function(res){
      var column = [
        {data:"namaMitra"},
        {data:"alamat"},
        {data:"pic"},
        {data:"noHP"},
        {data:"email"},
        {
            "defaultContent": "<button id='disposeTombol' class='btn btn-block btn-primary'>Lihat</button>"
        }
      ]
  
      var table = dataTables(res, column)
      $('#example3 tbody').on( 'click', 'button', function () { 
        var davitas
        var datas = table.row( $(this).parents('tr') ).data();
        var vitas = table.row( $(this)).data();
        if(typeof datas=='undefined'){
          davitas=vitas;
        }else if(typeof vitas=='undefined'){
          davitas=datas;
        }
        document.cookie = "idMitra=" + davitas.id;
        url="/detailmitra";
        window.location=url;
      });
    })
  }

  function showUserTable(){
    getIcon().then(function(res){
        var column = [
          {data:"usersByUsername.username"},
          {data:"namaLengkap"},
          {data:"email"},
          {data:"jabatan"},
          {data:"usersByUsername.authoritiesByUsername[0].authority"},
          {data:"id"},
        ]
        dataTables(res,column)
      })
}