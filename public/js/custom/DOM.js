// const { finished } = require("form-data");

userSettingEdit()
cekPekerjaanUser();
var userCount = 0;
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

function openDetail(nomor){
    document.cookie = "idDetail=" + nomor;
    url="/detailpekerjaan";
    window.location=url;
    console.log(nomor)
}

function addMitraPerson(){
    userCount++
    console.log("TARJON REEEEEEEEEEE")
    $("#mitraPerson").append(
        $('<div class="form-group">').append(
            $('<label for="exampleInputPassword1">Nama Anggota</label>'),
            $('<input type="text" class="form-control" id="namatim" placeholder="Masukkan Nama" name="namaAnggota">')
        ),
        $('<div class="form-group">').append(
            $('<label for="exampleInputEmail1">No. HP</label>'),
            $("<input type='text' class='form-control' id='namatim' data-inputmask='mask': '999999999999' data-mask placeholder='Masukkan Kontak' name='kontakAnggota'>")
        ),
        $('<div class="form-group">').append(
            $('<label for="exampleInputPassword1">E-mail</label>'),
            $('<input type="email" class="form-control" id="namatim" placeholder="Masukkan E-mail" name="emailAnggota">')
        )
    )
}

$('#loginForm').submit(function(e){
    e.preventDefault();
    var data = $(this).serializeFormJSON();
    console.log(data)
    document.cookie = "username=" + data.username;
    $.ajax({
        type:"POST",
        dataType: "json",
        data:data,
        url:"/login",
        success:function(data)
        {
            url="/home"
            window.location=url
        }
      });
    // login(data)
})

$('#formAnggota').submit(function (e) {
    e.preventDefault();
    var data = $(this).serializeFormJSON();
    console.log(data)
    // var key = getCookie("token");
    var dataPerson=[];
    // var namaPerson;
    // console.log(data)
    if(userCount==0){
        var anggotaData={
            "noHp": data.kontakAnggota,
            "namaPerson": data.namaAnggota,
            "email": data.emailAnggota
        }
        dataPerson.push(anggotaData)
    }else{
        for(let i = 0; i <= userCount; i++){
            var anggotaData={
                "noHp": data.kontakAnggota[i],
                "namaPerson": data.namaAnggota[i],
                "email": data.emailAnggota[i]
            }
            dataPerson.push(anggotaData)    
        }
    }
    console.log(dataPerson)
    var dataOrang =     {
        "namaMitra": data.namaMitra,
        "alamat": data.alamat,
        "jabatan": data.jabatan,
        "usernameByMitra": {
            "username": data.username
        },
        "noHP": data.kontakMitra,
        "email": data.emailMitra,
        "pic": data.picMitra,
        "personData": dataPerson
    }
    console.log(dataOrang)
    $('.btn').prop('disabled', true)
    sendData(dataOrang)
});

$('#formUser').submit(function (e) {
    e.preventDefault();
    var data = $(this).serializeFormJSON();
    console.log(data)
    let dataUser = {
        "email": data.emailUser,
        "namaLengkap": data.namaUser,
        "noHP": data.kontakUser,
        "wilayahKerja": data.wilayahKerja,
        "jabatan": data.jabatanUser,
        "usersByUsername": {
            "username": data.username
        },
        "role":data.options
    }
    console.log(dataUser)
    $('.btn').prop('disabled', true)
    sendDataUser(dataUser)
})

$('#tambahAnggota').submit(function (e) {
    e.preventDefault();
    var data = $(this).serializeFormJSON();
    console.log(data)
    var idMitra = getCookie('idMitra')
    let dataAnggota = 
        {
            "noHp": data.kontakAnggota,
            "namaPerson": data.namaAnggota,
            "email": data.emailAnggota,
            "mitrapersonById": {
                "id": idMitra,
            }
        }
    
    $('.btn').prop('disabled', true)
    sendDataAnggota(dataAnggota)
})

function submitQC(){
    var id = getCookie("idPekerjaan")
    var pekerjaan = getCookie("idDetail")
    var username = getCookie("username")
    console.log("aekhgaer",id)
    if(pekerjaan=="Survei"){
        approveSurvei(id, username);
    }else if(pekerjaan=="Verifikasi"){
        approveVerifikasi(id, username);
    }else if(pekerjaan=="Laporan Aktivasi"){
        approveLaporan(id, username);
    }else if(pekerjaan=="Bai Fot"){
        approveBaiFot(id, username);
    }
    $('#submitQC').prop("disabled",true)
}

function revisiQC(){
    var id = getCookie("idPekerjaan")
    var pekerjaan = getCookie("idDetail")
    var username = getCookie("username")
    console.log("nad sigger",id)
    if(pekerjaan=="Verifikasi"){
        revisiVerifikasi(id, username);
    }else if(pekerjaan=="Laporan Aktivasi"){
        revisiLaporan(id, username);
    }
}

$('#userSettingForm').submit(function(e){
     e.preventDefault();
    var data = $(this).serializeFormJSON();
    console.log(data)
    changePassword(data)
})

$('#userSetting').click(function(){
    document.cookie = "idSettingCard=setting"
    window.location='/userSetting'
})

$('#logout').click(function(){
    console.log("ERERERRE")
    document.cookie = "cookieToken= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idDetail= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idMitra= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idDispose= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idCrm= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idPekerjaan= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idSettingCard= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "username= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "cookieRoles= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    window.location='/login'
})

$('#logoutCard').click(function(){
    console.log("ERERERRE")
    document.cookie = "cookieToken= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idDetail= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idMitra= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idDispose= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idCrm= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idPekerjaan= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "idSettingCard= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "username= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    document.cookie = "cookieRoles= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    window.location='/login'
})

function showPassword(id){
    $('#mata').attr('class','far fa-eye-slash')
}

async function userSettingEdit(){
    console.log(getCookie("cookieRoles")!='j:["ROLE_MITRA"]')
    if(getCookie("cookieRoles")!='j:["ROLE_MITRA"]'){
        const res=await axios.get(Link.cekUserIcon+getCookie("username"))
        .then(res=>{
            $('#penggunaCard').text(res.data.usersByUsername.username)
            $('#username').val(res.data.usersByUsername.username)
            $('#email').val(res.data.email).prop('disabled',true)
            $('#name').val(res.data.namaLengkap).prop('disabled',true)
            $('#roles').text(res.data.usersByUsername.authoritiesByUsername[0].authority).prop('disabled',true)
            $('#roleCard').text(res.data.usersByUsername.authoritiesByUsername[0].authority);
        })
    }else{
        const res=await axios.get(Link.cekUserMitra+getCookie("username"))
        .then(res=>{
            $('#penggunaCard').text(res.data.usernameByMitra.username)
            $('#username').val(res.data.usernameByMitra.username)
            $('#email').val(res.data.email).prop('disabled',true)
            $('#name').val(res.data.namaMitra).prop('disabled',true)
            $('#roles').text(res.data.usernameByMitra.authoritiesByUsername[0].authority).prop('disabled',true)
            $('#roleCard').text(res.data.usernameByMitra.authoritiesByUsername[0].authority);
        })
    }
}

async function getInviteAsset(){
    const res = await axios.get(Link.getInvite+getCookie("username"))
    .then(res=>{
    console.log(res)
    $('#invites').empty()
    $.each(res.data,function(index,value){
    $('#invites').append(
        $('<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="invites">'),
                $('<a href="#" class="dropdown-item">').append(
                    $('<div class="media">').append(
                        $('<img src="https://cdn.discordapp.com/emojis/879760702733172797.webp?size=128&quality=lossless" alt="User Avatar" class="img-size-50 mr-3 img-circle">'),
                        $('<div class="media-body">').append(
                            $('<h3 class="dropdown-item-title">').append(
                                $('<p>').text(value.invitePerson+" telah mengundang anda ke "+value.paByInvite.crmById.noPA)
                            ),
                            $('<button type="button" class="btn btn-primary">').text("TERIMA").on('click',function(e){terimaInvite(value.id)}),
                            $('<span>   </span>'),
                            $('<button type="button" class="btn btn-danger">').text("TOLAK")
                            // $('<p class="text-sm">').text(value.notif),
                            // $('<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i>'+waktuNotif+'</p>')
                        )
                    )
                )
            )
        })
    })
  }

async function cekPekerjaanUser(){
    const res=await axios.get(Link.getPekerjaanUser+getCookie("username"))
    .then(res=>{
        console.log(res)
        $('#profilOpenPA').text(res.data.OpenPa)
        $('#profilClosedPA').text(res.data.ClosedPa)
        $('#profilVerifProgress').text(res.data.VerificationOnProgress)
    })
}

function klikNotif(e){
    document.cookie = "idSettingCard=notif"
    window.location='/userSetting'
}

function kedispose(){
    url="/disposeAsset";
    window.location=url;
}