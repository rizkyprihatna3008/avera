class Link extends String{
    constructor(){
        super("http://127.0.0.1:99");
    }
    
    // Link Crm
    Crm(){
        return this.toString() + "/crm/pa/"
    }
    CrmAja(){
        return this.toString() + "/crm/"
    }
    uploadCrm(){
        return this.toString() + "/crm/uploadCrm"
    }
    uploadCRM2(){
        return this.toString() + "/crm/uploadCrm2"
    }
    // End Link Crm

    // Link Attach
    surveiCrm(){
        return this.toString() + "/attach/surveiCrm"
    }
    verifikasiCrm(){
        return this.toString() + "/attach/verifikasiCrm"
    }
    baiCrm(){
        return this.toString() + "/attach/baiCrm"
    }
    laporanCrm(){
        return this.toString() + "/attach/laporanCrm"
    }
    verifikasi(){
        return this.toString() + "/attach/verifikasi"
    }
    approveSurvei(){
        return this.toString() + "/attach/updateData/approveSurvei/"
    }
    approveVerifikasi(){
        return this.toString() + "/attach/updateData/approveVerifikasi/"
    }
    approveLaporan(){
        return this.toString() + "/attach/updateData/approveLaporan/"
    }
    approveBaiFot(){
        return this.toString() + "/attach/updateData/approveBaiFot/"
    }
    revisiVerifikasi(){
        return this.toString() + "/attach/editData/revisiVerifikasi/"
    }
    revisiLaporan(){
        return this.toString() + "/attach/editData/revisiLaporan/"
    }
    attachProgress(){
        return this.toString() + "/attach/progress/"
    }
    surveisave(){
        return this.toString() + "/attach/survei/save"
    }
    submitLaporan(){
        return this.toString() + "/attach/sendData/uploadLaporan"
    }
    submitBom(){
        return this.toString() + "/attach/sendData/uploadSurvei"
    }
    submitverifikasi(){
        return this.toString() + "/attach/sendData/uploadVerifikasi"
    }
    submitTestbai(){
        return this.toString() + "/attach/sendData/uploadBaiFot"
    }
    // End Link Attach

    // Link Disposisi
    disposeMitra(){
        return this.toString() + "/disposisiMitra"
    }
    disposeMitrasave(){
        return this.toString() + "/disposisiMitra/save"
    }
    asset(){
        return this.toString() + "/disposisiMitra/getAsset/"
    }
    verifikasiAsset(){
        return this.toString() + "/disposisiMitra/submitAsset"
    }
    inviteAsset(){
        return this.toString() + "/disposisiMitra/inviteAsset"
    }
    terimaInvite(){
        return this.toString() + "/disposisiMitra/acceptInvite/"
    }
    // End Link Disposisi

    // Link History
    historyList(){
        return this.toString() + "/history/list/"
    }
    historyListDetail(){
        return this.toString() + "/history/listDetail/"
    }
    // End Link History

    // Link Dashboard
    dashboard(){
        return this.toString() + "/dashboard/crm"
    }
    dashboard(){
        return this.toString() + "/dashboard/crm/"
    }
    // End Link Dashboard

    // Link Upload
    baiCrm(){
        return this.toString() + "/upload/BaiCrm"
    }
    fotCrm(){
        return this.toString() + "/upload/fotCrm"
    }
    fotCrm(){
        return this.toString() + "/upload/FotCrm"
    }
    // End Link Upload

    // Link UploadFile
    downloadFile(){
        return this.toString() + "/uploadFile/downloadFile/"
    }
    UploadFile(){
        return this.toString() + "/uploadFile/uploadBanyak"
    }
    // End Link UploadFile

    // Link User
    userMitraSave(){
        return this.toString() + "/user/create/mitra/save"
    }
    userPenggunaSave(){
        return this.toString() + "/user/create/pengguna/save"
    }
    changeUserPasswordSetting(){
        return this.toString() + "/user/edit/changeUserPassword/"
    }
    changeUserProfilePicture(){
        return this.toString() + "/user/edit/addProfilePicture/"
    }
    // End Link User

    
    login(){
        return this.toString() + "/auth/signin"
    }
    mitra(){
        return this.toString() + "/mitra/"
    }
    icon(){
        return this.toString() + "/icon/"
    }
    tambahAnggota(){
        return this.toString() + "/person/addPerson/"
    }
    reply(){
        return this.toString() + "/manage/replyPekerjaan/"
    }
    getReply(){
        return this.toString() + "/manage/getReply/"
    }
}
module.exports = { Link };