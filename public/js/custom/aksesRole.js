var role = getCookie("cookieRoles")
var pengguna = getCookie("username")
var sidefoto = getCookie("cookiePicture")
console.log("HAY", role)
if(role=="tidak ada"){
    window.location='/login'
}
if(sidefoto=="j:null"){
    sidefoto=null
}
$('#fotoSidebar').attr("src",Link.profilePicture+sidefoto)
$('#profilFoto').attr("src",Link.profilePicture+sidefoto)
$('#pengguna').text(pengguna)
if(role=='j:["ROLE_MITRA"]'){
    $('#disposeAsset').hide()
    $('#disposePA').hide()
    $('#manageUser').hide()
    $('#tambahMitraPage').hide()
    $('#uploadDokumenButton2').text("-").prop('disabled', true)
}else if(role=='j:["ROLE_PTL"]'){
    $('#manageUser').hide()
    $('#manageMitra').hide()
    $('#uploadCRMPage').hide()
    $('#revisiQC').show()
    $('#submitQC').show()
}else if(role=='j:["ROLE_ASSET"]'){
    $('#disposeAsset').hide()
    $('#disposePA').hide()
    $('#manageUser').hide()
    $('#manageMitra').hide()
    $('#uploadDokumenButton').text("-").prop('disabled', true)
    $('#uploadDokumenButton3').text("-").prop('disabled', true)
    $('#uploadDokumenButton4').text("-").prop('disabled', true)
}else if(role=='j:["ROLE_TIM_QC"]'){
    $('#disposePA').hide()
    $('#manageUser').hide()
    $('#manageMitra').hide()
    $('#disposeAsset').hide()
    $('#revisiQC').show()
    $('#submitQC').show()
    $('#uploadDokumenButton').text("-").prop('disabled', true)
    $('#uploadDokumenButton3').text("-").prop('disabled', true)
    $('#uploadDokumenButton4').text("-").prop('disabled', true)
}else{
    $('#revisiQC').show()
    $('#submitQC').show()
}