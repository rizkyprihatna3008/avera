var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
var file;
var bomSurvei;
var gdbSurvei;
var dokumentasiSurvei;
var datacoreVerifikasi;
var gdbVerifikasi;
var catatanVerifikasi;
var bomLaporan;
var boqLaporan;
var laporanAktivasi;
var datacoreLaporan;
var fileTestcom;
var fileBai;
var tipeDokumen;
var crmId = getCookie("idCrm");
var disposeId = getCookie("idDispose");

function getDokumen(e, tipe){
    file = (e.target.files);
    var files = [];
    $.each(file, function(index, value){
        console.log(value)    
        if(file.length < 2){
            file = value
            console.log(file)
            if(tipe == 3){
                dokumentasiSurvei = file;
            }
        }else{
            dokumentasiSurvei = files;
            files.push(value)
        }

    })
    console.log(dokumentasiSurvei)
    tipeDokumen=tipe
    if(tipe == 1){
        bomSurvei = file;
    }else if(tipe == 2){
        gdbSurvei = file;
    }else if(tipe == 4){
        datacoreVerifikasi = file;
    }else if(tipe == 5){
        gdbVerifikasi = file;
    }else if(tipe == 6){
        catatanVerifikasi = file;
    }else if(tipe == 7){
        bomLaporan = file;
    }else if(tipe == 8){
        boqLaporan = file;
    }else if(tipe == 9){
        laporanAktivasi = file;
    }else if(tipe == 10){
        datacoreLaporan = file;
    }else if(tipe == 11){
        fileTestcom = file;
    }else if(tipe == 12){
        fileBai = file;
    }
    console.log("tipe", tipe);
}

// async function uploadCrm(){
//     const formData = new FormData();
//     formData.append('excel',file);

//     try{
//         const res = await axios.post(Link.uploadCrm,formData)
//         .then(res=>{
//             console.log(res)
//         })    
//     }
//     catch(err){
//         console.log(err)
//     }
//     console.log("upload image",res);
//     location.reload();
//     return res;
// }

function openModal(dokumen){
    console.log(tipeDokumen)
    if(dokumen == 1){
        url=Link.submitBom
        if(tipeDokumen == 1){
            var uploadFile = {
                "surveiBom": bomSurvei.name,
                "catatan":$("#catatan1").val()
            }
            var mitra = {
                "mitraBySurvei": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 2){
            var uploadFile = {
                "gdb": gdbSurvei.name,
                "catatan":$("#catatan2").val()
            }
            var mitra = {
                "mitraBySurvei": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 3){
            if(dokumentasiSurvei.length > 1){
                var uploadFile = {
                    "surveiByFoto":[],
                    "catatan":$("#catatan3").val()
                }
                $.each(dokumentasiSurvei, function(index, value){
                    var fotoo = {
                        "fileName":value.name
                    }
                    uploadFile.surveiByFoto.push(fotoo)
                })
            }else{
                var uploadFile = {
                    "surveiByFoto":[],
                    "catatan":$("#catatan3").val()
                }
                var fotoo = {
                    "fileName":dokumentasiSurvei.name
                }
                uploadFile.surveiByFoto.push(fotoo)
            }

            var mitra = {
                "mitraBySurvei": {
                    "id": getCookie("idMitra")
                }
            }
        }
    }else if(dokumen == 2){
        url=Link.submitVerifikasi
        if(tipeDokumen == 4){
            var uploadFile = {
                "dataCore": datacoreVerifikasi.name,
                "catatan":$("#catatan4").val()
            }
            var mitra = {
                "mitraByVerifikasi": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 5){
            var uploadFile = {
                "gdb": gdbVerifikasi.name,
                "catatan":$("#catatan5").val()
            }
            var mitra = {
                "mitraByVerifikasi": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 6){
            var uploadFile = {
                "catatanVerifikasi": catatanVerifikasi.name,
                "catatan":$("#catatan6").val()
            }
            var mitra = {
                "mitraByVerifikasi": {
                    "id": getCookie("idMitra")
                }
            }
        }
    }else if(dokumen == 3){
        url=Link.submitLaporan
        if(tipeDokumen == 7){
            var uploadFile = {
                "bom": bomLaporan.name,
                "catatan":$("#catatan7").val()
            }
            var mitra = {
                "mitraByLaporan": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 8){
            var uploadFile = {
                "boq": boqLaporan.name,
                "catatan":$("#catatan8").val()
            }
            var mitra = {
                "mitraByLaporan": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 9){
            var uploadFile = {
                "laporan1": laporanAktivasi.name,
                "catatan":$("#catatan9").val()
            }
            var mitra = {
                "mitraByLaporan": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 10){
            var uploadFile = {
                "dataCore": datacoreLaporan.name,
                "catatan":$("#catatan10").val()
            }
            var mitra = {
                "mitraByLaporan": {
                    "id": getCookie("idMitra")
                }
            }
        }
    }else if(dokumen == 4){
        url=Link.submitTestbai
        if(tipeDokumen == 11){
            var uploadFile = {
                "testCom": fileTestcom.name,
                "catatan":$("#catatan11").val()
            }
            var mitra = {
                "mitraByBaiFot": {
                    "id": getCookie("idMitra")
                }
            }
        }else if(tipeDokumen == 12){
            var uploadFile = {
                "bai": fileBai.name,
                "catatan":$("#catatan12").val()
            }
            var mitra = {
                "mitraByBaiFot": {
                    "id": getCookie("idMitra")
                }
            }
        }
    }
    var data = {
        "noPA": {
            "id":disposeId
        },
        "submitter":getCookie("username")
    }
    console.log($(".form-control").val())
    const dataDokumen = {
        ...data,
        ...uploadFile,
        ...mitra
    };
    $('#pekerjaanButton').prop('disabled', true)
    $('#pekerjaanButton2').prop('disabled', true)
    $('#pekerjaanButton3').prop('disabled', true)
    $('#pekerjaanButton4').prop('disabled', true)
    submitDokumen(dataDokumen,url)
}

async function submitDokumen(dokumen, url){
    const formData = new FormData();
    var fols = []
    if(file.length>1){
        $.each(file, function(index, value){
            formData.append('image',value);
        })
    }else{
        formData.append('image',file);
    }
    formData.append('body', JSON.stringify(dokumen))
    formData.append('url', url)
    console.log(formData)

    $.ajax({
        type: "POST",
        url: "/uploadFols",
        data: formData,
        processData: false,
        contentType: false,
        success: function(r){
            Toast.fire({
                icon: 'success',
                title: ' ' + "Upload Berhasil"
            })
            $('#pekerjaanButton').prop('disabled', false)
            $('#pekerjaanButton2').prop('disabled', false)
            $('#pekerjaanButton3').prop('disabled', false)
            $('#pekerjaanButton4').prop('disabled', false)
            console.log("result",r)
        },
        error: function (e) {
            Toast.fire({
                icon: 'error',
                title: ' ' + "Terjadi Kesalahan"
            })
            $('#pekerjaanButton').prop('disabled', false)
            $('#pekerjaanButton2').prop('disabled', false)
            $('#pekerjaanButton3').prop('disabled', false)
            $('#pekerjaanButton4').prop('disabled', false)
            console.log("some error", e);
        }
    });
}
