const { Link } = require("./variable");
const express = require('express');
var FormData = require('form-data');
var engines = require('consolidate');
const multer = require('multer')
const axios = require('axios').default;
var Stomp = require('stompjs')
var SockJS = require('sockjs-client');
const cookieParser = require('cookie-parser');
const { response } = require('express');
const { cookie } = require('express/lib/response');
const formidable = require('express-formidable');
const { promisify } = require('util')
const app = express();
var fs = require('fs');
const unlinkAsync = promisify(fs.unlink)
var bodyParser= require('body-parser');
const cons = require("consolidate");
const fileStorageEngine = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null,'./images')
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname)
        },
    });
    const upload = multer ({storage : fileStorageEngine}) 
// rendering html
app.set('views', __dirname + '/views');
app.engine('html', engines.mustache);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

// var url = 'http://139.59.47.25:98/our-websocket';
// console.log('connecting to '+url);
// var sock= new SockJS(url);
// var stompClient = Stomp.over(sock);

// var callback = function(message){
//         onsole.log('received message: '+message);
// };
      
      
// sock.onopen = function() {
//         console.log('open');
// };

// sock.onmessage = function(e) {
//         console.log('message', e.data);
//         sock.close();
// };

// sock.onclose = function() {
//         console.log('close');
// };
      
      
// stompClient.connect({},function (frame) {
//         console.log("TEST")
//         stompClient.subscribe('/topic/message', callback);
// // stompClient.send('/message',{},'{"message":"from nodejs"}');
// });
// app.use(formidable());

// function parseCookies (request) {
//         var list = {},
//             rc = request.headers.cookie;
    
//         rc && rc.split(';').forEach(function( cookie ) {
//             var parts = cookie.split('=');
//             list[parts.shift().trim()] = decodeURI(parts.join('='));
//         });
    
//         return list;
//     }


// bwt cssdss
function validatedCookie (req, res, next) {
        const {cookies} = req;
        if ('cookieToken' in cookies){
            console.log("ada tokennya mas")
        }
        else res.status(403).send('Kurang cepat')
        next();
    }
app.use(express.static("public"))
app.get('/', function (request, response) {
        console.log("HEY")
        response.send("index")
});
app.get("/detailhistory", (req,res) => {
        res.render("pages/History/detailHistory")
})
app.get("/history", (req,res) => {
        res.render("pages/History/history")
})
app.get("/userSetting", (req,res) => {
        res.render("pages/User/profile")
})
app.get("/dispose", (req,res) => {
        res.render("pages/Manage Pekerjaan/Disposisi PA/disposePA")
})
app.get("/pekerjaan", (req,res) => {
        res.render("pages/Manage Pekerjaan/Disposisi PA/pekerjaan")
})
app.get("/detailpekerjaan", (req,res) => {
        res.render("pages/Manage Pekerjaan/Pekerjaan/detailPekerjaan")
})
app.get("/reportPA", (req,res) => {
        res.render("pages/Manage Pekerjaan/Pekerjaan/reportPA")
})
app.get("/uploadpekerjaan", (req,res) => {
        res.render("pages/Manage Pekerjaan/Pekerjaan/uploadPekerjaan")
})
app.get("/user", (req,res) => {
        res.render("pages/Manage User/user")
})
app.get("/mitra", (req,res) => {
        res.render("pages/ManageMitra/mitra")
})
app.get("/detailmitra", (req,res) => {
        res.render("pages/ManageMitra/detailmitra")
})
app.get("/disposeAsset", (req,res) => {
        res.render("pages/Manage Pekerjaan/Disposisi PA/disposeAsset")
})
app.get("/home", (req,res) => {
        var stompClient = null;
        var socket = new SockJS('http://139.59.47.25:98/our-websocket');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function(frame){
                console.log('Connected To', frame);
                stompClient.subscribe('/topic/message', function(message){
                        console.log(JSON.parse(message.body).content);
                })
        })
        res.render("index")
})

// app.get("/ws", (req,res) => {
//         const ws = new WebClient('ws://139.59.47.25:98/our-websocket')
// })

app.get("/login", (req,res) => {
        res.render("pages/Login/login")
})

app.get('/crmApi',validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.Crm()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});

app.get('/crmIdApi/:crmId/',validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.CrmAja()
        var idCrm = req.params.crmId
        const response = axios.get(Linkapi+idCrm, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});

app.get('/HistorylistApi', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.historyList()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});

app.get('/mitraApi/', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.mitra()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});

app.get('/mitraApiId/:idMitra', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var idMi = req.params.idMitra
        var linkObj = new Link();
        var Linkapi = linkObj.mitra()
        const response = axios.get(Linkapi+idMi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});

app.get('/iconApi', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.icon()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});
app.get('/attachProgressApi/:crmId/:idDetail', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        console.log(cookies.cookieToken)
        var linkObj = new Link();
        var Linkapi = linkObj.attachProgress()
        var crmIds = req.params.crmId
        var idDetails = req.params.idDetail
        const response = axios.get(Linkapi+crmIds+"/"+idDetails, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});
app.get('/historyListDetailApi/:idHistory', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        console.log(cookies.cookieToken)
        var linkObj = new Link();
        var Linkapi = linkObj.historyListDetail()
        var historyids = req.params.idHistory
        const response = axios.get(Linkapi+historyids, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});
app.get('/downloadFileApi/:fileName', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        console.log(cookies.cookieToken)
        var linkObj = new Link();
        var Linkapi = linkObj.downloadFile()
        var filenames = req.params.fileName
        const response = axios.get(Linkapi+filenames, config)
        .then(ress=>{
                console.log(ress)
                return res.status(200).send({result: ress.data})
        })
        } catch (error) {
            res.send(error);
        }
});
app.get('/assetApi', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        console.log(cookies.cookieToken)
        var linkObj = new Link();
        var Linkapi = linkObj.asset()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});
app.get('/dashboardApi', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.dashboard()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});

app.get('/getReplyApi/:idPekerjaan', validatedCookie, function(req, res) {
        const {cookies} = req;
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.getReply()
        var idPekerjaan = req.params.idPekerjaan
        const response = axios.get(Linkapi+idPekerjaan, config)
        .then(ress=>{
                res.send(ress.data);
        })
        } catch (error) {
            res.send(error);
        }
});


app.post("/login", (request, response) =>{
        var username = request.body.username;
        var password = request.body.password
            var linkObj = new Link();
            var Linkapi = linkObj.login()
            const res = axios.post(Linkapi, 
            {"username":username,"password":password}
        )
        .then(rescookie=>{
            console.log(rescookie.data)
            response.cookie('cookieToken',rescookie.data.token, {maxAge: 36000000})
            response.cookie('cookieRoles',rescookie.data.role, {maxAge: 36000000})
            response.cookie('cookiePicture',rescookie.data.profilePicture, {maxAge: 36000000})
            return response.status(200).send({result: 'redirect', url:'/'})
        })
        .catch(function(error){
                return response.status(500).send({result: error})
        });
});
app.post('/uploadCrmApi', multer().single('file'), function(req, res) {
        const config = {
                headers: { Authorization: `Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJpY29uIGFkbWluIiwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJpYXQiOjE2NDA1OTIzOTIsImV4cCI6MTY0MDYyODM5Mn0.Ix5yLNFM6h2ECIF69VAOjs5oo1cQsa9WO1fEj_p5-7A` }
        };
        var linkObj = new Link();
        var Linkapi = linkObj.uploadCrm()
        console.log(req.files)
        // console.log(req.files)
        // console.log(req.body)k
        const response = axios.get(Linkapi, req.files, config)
        .then(ress=>{
                res.send(ress.data);
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});
app.post('/submitDokumenApi', function(req, res) {
        const {cookies} = req;
        console.log(req.data)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.submitDokumen()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});
app.post('/submitDisposeApi', function(req, res) {
        const {cookies} = req;
        console.log(req.data)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.submitDispose()
        const response = axios.get(Linkapi, config)
        .then(ress=>{
                res.send(ress.data);
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});

app.post('/approveSurvei', function(req, res) {
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.approveSurvei()
        const formData = new FormData();
        formData.append('username', req.body.username)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.id, formData, config)
        .then(ress=>{
                console.log(ress)
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                console.log(error)
                return res.status(500).send({result: error})
        })
});


app.post('/approveVerifikasi', function(req, res) {
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.approveVerifikasi()
        const formData = new FormData();
        formData.append('username', req.body.username)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.id, formData, config)
        .then(ress=>{
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});

app.post('/approveLaporan', function(req, res) {
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.approveLaporan()
        const formData = new FormData();
        formData.append('username', req.body.username)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.id, formData, config)
        .then(ress=>{
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});

app.post('/approveBaiFot', function(req, res) {
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.approveBaiFot()
        const formData = new FormData();
        formData.append('username', req.body.username)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.id, formData, config)
        .then(ress=>{
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});

app.post('/revisiVerifikasi', function(req, res) {
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.revisiVerifikasi()
        const formData = new FormData();
        formData.append('username', req.body.username)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.id, formData, config)
        .then(ress=>{
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});

app.post('/revisiLaporan', function(req, res) {
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.revisiLaporan()
        const formData = new FormData();
        formData.append('username', req.body.username)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.id, formData, config)
        .then(ress=>{
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
});

app.post("/verifikasiAssetApi", (request, response) =>{
        const {cookies} = request;
        // var linkObj = new Link();
        // var Linkapi = linkObj.inviteAsset()
        var data = {
                "assetByInvite":{
                "id":request.body.idAsset
            },
                "paByInvite":{
                "id":request.body.noPa
            },
                "invitePerson":request.body.username
            }
        // const formData = new FormData();
        // formData.append('idAsset', request.body.idAsset)
        // formData.append('noPa', request.body.noPa)
        // console.log(formData)
        const config = {
                headers: { 
                        Authorization: `Bearer ${cookies.cookieToken}`
                        // 'Content-Type': `multipart/form-data; boundary=${formData._boundary}`
                 }
        }
            var linkObj = new Link();
            var Linkapi = linkObj.inviteAsset()
            const res = axios.post(Linkapi,data, config
        )
        .then(rescookie=>{
                console.log(rescookie)
            return response.status(200).send({result: rescookie.data})
        })
        .catch(function(error){
                console.log(error)
                return response.status(500).send({result: error})
        })
        
        });
app.post("/tambahAnggotaApi", (request, response) =>{
        const {cookies} = request;
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.tambahAnggota()
        console.log(request)
            const res = axios.post(Linkapi,request.body, config
        )
        .then(rescookie=>{
        console.log(rescookie.data)
            return response.status(200).send({result: rescookie.data})
        })
        .catch(function(error){
                return response.status(500).send({result: error})
        })
});


app.post("/disposeMitrasaveApi", (request, response) =>{
        const {cookies} = request;
        console.log(request.body)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.disposeMitrasave()
            const res = axios.post(Linkapi,request.body, config
        )
        .then(rescookie=>{
                console.log(rescookie)
            return response.status(200).send({result: rescookie.data})
        })
        .catch(function(error){
                return response.status(500).send({result: error})
        })
});
app.post("/userMitraSaveApi", (request, response) =>{
        const {cookies} = request;
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.userMitraSave()
        try{
            const res = axios.post(Linkapi,request.body, config
        )
        .then(rescookie=>{
        console.log(rescookie.data)
            return response.status(200).send({result: rescookie.data})
        });
        } catch(error) {
            return response.status(401).send({error: "Something is wrong."})
        }
});
app.post("/userPenggunaSaveApi", (request, response) =>{
        const {cookies} = request;
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.userPenggunaSave()
            const res = axios.post(Linkapi,request.body
        )
        .then(rescookie=>{
        console.log(rescookie.data)
            return response.status(200).send({result: rescookie.data})
        })
        .catch(function(error){
                return response.status(500).send({result: error})
        })
});
app.post("/replyApi", (request, response) =>{
        const {cookies} = request;
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}` }
        }
        var linkObj = new Link();
        var Linkapi = linkObj.reply()
        console.log(request.body)
            const res = axios.post(Linkapi,request.body, config
        )
        .then(rescookie=>{
        console.log(rescookie.data)
            return response.status(200).send({result: rescookie.data})
        })
        .catch(function(error){
                return response.status(500).send({result: error})
        })
});

app.post('/changeUserPassword', function(req, res) {
        console.log(req.body)
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.changeUserPasswordSetting()
        const formData = new FormData();
        formData.append('oldPassword', req.body.oldPassword)
        formData.append('newPassword', req.body.newPassword)
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.body.username, formData, config)
        .then(ress=>{
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                console.log(error)
                return res.status(500).send({result: error})
        })
});

app.post('/acceptInvite/:id/', function(req, res) {
        console.log(req.params.id)
        const {cookies} = req
        var linkObj = new Link();
        var Linkapi = linkObj.terimaInvite()
        const formData = new FormData();
        const config = {
                headers: { Authorization: `Bearer ${cookies.cookieToken}`,
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
        }
        const response = axios.put(Linkapi+req.params.id, formData, config)
        .then(ress=>{
                console.log(ress)
                return res.status(200).send({result: ress.data})
        })
        .catch(function(error){
                console.log(error)
                return res.status(500).send({result: error})
        })
});

app.post('/uploadFol', upload.single('image'), async (req, res) =>{
        const {cookies} = req;
        const formData = new FormData();
        console.log(req.body.body)
        formData.append('file', fs.createReadStream(`./images/${req.file.filename}`))
        formData.append('body', JSON.stringify(JSON.parse(req.body.body)))
        const config = {
                headers: { 'Content-Type': `multipart/form-data; boundary=${formData._boundary}`, Authorization: `Bearer ${cookies.cookieToken}`  }
        }
                const response = axios.post(req.body.url, formData, config)
                .then(ress=>{
                        console.log(ress)
                        unlinkAsync(req.file.path)
                        return res.status(200).send({result: ress.data})
                })
        .catch(function(error){
                return res.status(500).send({result: error})
        })
        
});

app.post('/uploadTest', upload.single('file'), async (req, res) =>{
        const {cookies} = req;
        const formData = new FormData();
        formData.append('file', fs.createReadStream(`./images/${req.file.filename}`))
        const config = {
                headers: { 'Content-Type': `multipart/form-data; boundary=${formData._boundary}`, Authorization: `Bearer ${cookies.cookieToken}`  }
        }
                var linkObj = new Link();
                var Linkapi = linkObj.uploadCRM2
                const response = axios.post("http://139.59.47.25:98/crm/uploadCrm/", formData, config)
                .then(ress=>{
                        console.log(ress.data)
                        unlinkAsync(req.file.path)
                        return res.status(200).send({result: ress.data})
                })
                .catch(function(error){
                        return res.status(500).send({result: error})
                })
});

app.post('/uploadFols', upload.array('image'), async (req, res) =>{
        const formData = new FormData();
        var files = req.files
        console.log(req.body.url)
        files.forEach(element => formData.append('file', fs.createReadStream(`./images/${element.filename}`)))
        const {cookies} = req;
        formData.append('body', JSON.stringify(JSON.parse(req.body.body)))
                const config = {
                        headers: { 'Content-Type': `multipart/form-data; boundary=${formData._boundary}`, Authorization: `Bearer ${cookies.cookieToken}`  }
                }
                const response = axios.post(req.body.url, formData, config)
                .then(ress=>{
                        console.log(ress)
                        files.forEach(element => unlinkAsync(element.path))
                        return res.status(200).send({result: ress.data})
                })
                .catch(function(error){
                        console.log(error)
                        return res.status(500).send({result: error})
                })
});

app.post('/uploadProfil', upload.single('image'), async (req, res) =>{
        const {cookies} = req;
        console.log(req.body.username)
        const formData = new FormData();
        var linkObj = new Link();
        var Linkapi = linkObj.changeUserProfilePicture()
        formData.append('picture', fs.createReadStream(`./images/${req.file.filename}`))
        const config = {
                headers: { 'Content-Type': `multipart/form-data; boundary=${formData._boundary}`, Authorization: `Bearer ${cookies.cookieToken}`  }
        }
                const response = axios.put(Linkapi+req.body.username, formData, config)
                .then(ress=>{
                        console.log(ress)
                        res.cookie('cookiePicture',req.file.filename, {maxAge: 36000000})
                        unlinkAsync(req.file.path)
                        return res.status(200).send({result: ress.data})
                })
                .catch(function(error){
                        console.log(error)
                        return res.status(500).send({result: error})
                })
        
});
    
app.listen(4000, function () {
        console.log('Listening on port 4000');
});


