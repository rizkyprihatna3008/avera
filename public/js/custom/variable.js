var server = "http://127.0.0.1:99";

var Link = 
{
    // Link Attach
    surveiBatch: server+"/attach/surveiBatch/",
    surveiCrm: server+"/attach/surveiCrm/",
    verifikasiCrm: server+"/attach/verifikasiCrm/",
    verifikasi: server+"/attach/verifikasi/",
    laporanCrm: server+"/attach/laporanCrm/",
    attachProgress: server+"/attach/progress",
    submitBom: server+"/attach/sendData/uploadSurvei",
    surveiSave: server+"/attach/survei/save/",
    submitVerifikasi: server+"/attach/sendData/uploadVerifikasi/",
    submitTestbai: server+"/attach/sendData/uploadBaiFot",
    submitLaporan: server+"/attach/sendData/uploadLaporan/",
    detailSurvei: server+"/attach/surveiCrm/",
    detailVerifikasi: server+"/attach/verifikasiCrm/",
    detailLaporan: server+"/attach/laporanCrm/",
    detailBaifot: server+"/attach/baiFotCrm/",
    approveSurvei: server+"/attach/updateData/approveSurvei/",
    approveVerifikasi: server+"/attach/updateData/approveVerifikasi/",
    approveLaporan: server+"/attach/updateData/approveLaporan/",
    approveBaiFot: server+"/attach/updateData/approveBaiFot/",
    statusPekerjaan: server+"/attach/GetStatusPekerjaan/",
    // End Link Attach

    // Link Disposisi
    disposisiMitra: server+"/disposisiMitra/",
    disposisiMitraSave: server+"/disposisiMitra/save",
    mitraUsername: server+"/disposisiMitra/mitraUsername/",
    assetUsername: server+"/disposisiMitra/assetUsername/",
    asset: server+"/disposisiMitra/getAsset",
    verifikasiAsset: server+"/disposisiMitra/submitAsset",
    inviteAsset: server+"/disposisiMitra/inviteAsset",
    getInvite: server+"/disposisiMitra/getInvite/",
    acceptInvite: server+"/disposisiMitra/acceptInvite/",
    // End Link Disposisi

    // Link History
    historyList: server+"/history/list/",
    historyListDetail: server+"/history/listDetail/",
    // End Link History

    // Link Crm
    crm: server+"/crm/",
    crmNotDispose: server+"/crm/pa",
    uploadCrm: server+"/crm/uploadCrm/",
    // End Link Crm

    // Link Mitra
    mitra: server+"/mitra/",
    mitraByUsername: server+"/mitra/mitraByUsername/",
    batchTim: server+"/mitra/saveBatch/",
    cekUserMitra: server+"/mitra/mitraByUsername/",
    // End Link Mitra

    // Link UploadFile
    uploadFile: server+"/uploadFile/uploadBanyak/",
    downloadFile: server+"/uploadFile/downloadFile/",
    profilePicture: server+"/uploadFile/getProfilePicture/",
    // End Link UploadFile

    // Link Manage
    reply: server+"/manage/replyPekerjaan",
    // End Link Manage

    // Link Dashboard
    dashboard: server+"/dashboard/crm",
    // End Link Dashboard

    // Link User
    userMitraSave: server+"/user/create/mitra/save",
    userPenggunaSave: server+"/user/create/pengguna/save",
    notifikasi: server+"/user/notification/cekNotif/",
    getPekerjaanUser: server+"/user/notification/GetPekerjaanUser/",
    // End Link User

    // Link Icon
    icon: server+"/icon/",
    cekUserIcon: server+"/icon/user/",
    // End Link Icon

    // Link Person
    person: server+"/person/",
    tambahAnggota: server+"/person/addPerson",
    personUser: server+"/person/user/",
    personMitra: server+"/person/mitra/",
    // End Link Person

    // Link Misc
    login: server+"/auth/signin",
    uploadBom: server+"/bom/upload/",
    baiCrm: server+"/upload/BaiCrm/",
    fotCrm: server+"/upload/FotCrm/",
    baiCrm: server+"/upload/BaiCrm/",
    bomSurvei: server+"/bom/survei/",
    fotCrm: server+"/upload/FotCrm/",
    tim: server+"/tim/",
    datacoreVerif: server+"/datacore/verif/"
    // End Link Misc
}
