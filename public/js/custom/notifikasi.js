async function notifikasi(){
    var username = encodeURIComponent(getCookie("username").trim())
    console.log(Link.notifikasi + username)
    const res=await axios.get(Link.notifikasi+username)
    .then(res=>{
        $('#jumlahNotif').text(res.data.length)
        if(res.data.length==0){
            $('#jumlahNotif').empty()
            $('#notifikasi').empty()
            $('#notifikasi').append(
                $('<div class="dropdown-divider"></div>'),
                $('<a class="dropdown-item dropdown-footer">Tidak ada Notifikasi</a>').on('click',function(e){klikNotif();})
            )
        }else{
            $('#notifikasi').empty()
            $.each(res.data,function(index,value){
                var waktuNotif = konversiHari(new Date() - new Date(value.tanggalPostNotif))
                console.log(value)
                $('#notifikasi').append(
                    $('<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="notifikasi">'),
                    $('<a href="#" class="dropdown-item">').append(
                        $('<div class="media">').append(
                            $('<img src="https://cdn.discordapp.com/emojis/879760702733172797.webp?size=128&quality=lossless" alt="User Avatar" class="img-size-50 mr-3 img-circle">'),
                            $('<div class="media-body">').append(
                                $('<h3 class="dropdown-item-title">').append(
                                    $('<p>').text('Guz Agunk'),
                                    $('<span class="float-right text-sm text-danger"><i class="fas fa-star">')
                                ),
                                $('<p class="text-sm">').text(value.notif),
                                $('<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i>'+waktuNotif+'</p>')
                            )
                        )
                    )
                )          
            })
            $('#notifikasi').append(
                $('<div class="dropdown-divider"></div>'),
                $('<a href="/userSetting" class="dropdown-item dropdown-footer">Lihat Semua Notifikasi</a>').on('click',function(e){klikNotif();})
            )
        }
    })
}
notifikasi()

async function notifikasiSetting(){
    var username = encodeURIComponent(getCookie("username").trim())
    console.log(Link.notifikasi + username)
    $('#timelineSetting').empty()
    const res=await axios.get(Link.notifikasi+username+"?pageSize=100")
    .then(res=>{
        $.each(res.data,function(index,value){
            var waktuNotif = konversiHari(new Date() - new Date(value.tanggalPostNotif))
            $('#timelineSetting').append(
                $('<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="notifikasi">'),
                $('<a href="#" class="dropdown-item">').append(
                    $('<div class="media">').append(
                        $('<img src="https://cdn.discordapp.com/emojis/879760702733172797.webp?size=128&quality=lossless" alt="User Avatar" class="img-size-50 mr-3 img-circle">'),
                        $('<div class="media-body">').append(
                            $('<h3 class="dropdown-item-title">').append(
                                $('<p>').text('Guz Agunk'),
                                $('<span class="float-right text-sm text-danger"><i class="fas fa-star">')
                            ),
                            $('<p class="text-sm">').text(value.notif),
                            $('<p class="text-sm text-muted"><i class="far fa-clock mr-1"></i>'+waktuNotif+'</p>')
                        )
                    )
                )
            )
        })
    })
}
notifikasiSetting()