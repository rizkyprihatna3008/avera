function tanggalFormat(tanggalKomentar){
    var d = new Date(tanggalKomentar),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()
  
      var days = new Date(tanggalKomentar),
        month = '' + (days.getMonth() + 1),
        day = '' + days.getDate();
        year = days.getFullYear();
  
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
  
      month = days.toLocaleString('default', { month: 'short' })
      var tanggal = [day, month, year].join(' ')
      dateNew = tanggal
      var formattedTime = hours + ':' + minutes.substr(-2);
      return formattedTime
  }
  
  function hariKomentar(tanggalKomentar){
    var d = new Date(tanggalKomentar),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()
  
      var days = new Date(tanggalKomentar),
        month = '' + (days.getMonth() + 1),
        day = '' + days.getDate();
        year = days.getFullYear();
  
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
      month = days.toLocaleString('default', { month: 'short' })
      var tanggal = [day, month].join(' ')
      dateNew = tanggal
      var formattedTime = hours + ':' + minutes.substr(-2);
      return tanggal
  }
  
  function formatTanggal(tanggalKomentar){
    var d = new Date(tanggalKomentar),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()
  
      var days = new Date(tanggalKomentar),
        month = '' + (days.getMonth() + 1),
        day = '' + days.getDate();
        year = days.getFullYear();
  
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
      month = days.toLocaleString('default', { month: 'short' })
      var tanggal = [day, month, year].join(' ')
      dateNew = tanggal
      var formattedTime = hours + ':' + minutes.substr(-2);
      var combinedTime = [tanggal,formattedTime].join('-')
      return combinedTime
  }
  
  function konversiHari(t){
    var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor( (t - d * cd) / ch),
        m = Math.round( (t - d * cd - h * ch) / 60000),
        pad = function(n){ return n < 10 ? '0' + n : n; };
  if( m === 60 ){
    h++;
    m = 0;
  }
  if( h === 24 ){
    d++;
    h = 0;
  }
  var formattedTime = d + ' Hari ' + h + ' Jam ' + m + ' Menit.'
  if(t<=86400000){
    formattedTime = h + ' Jam ' + m + ' menit.';
  }else if(t>=86400000){
    formattedTime = d + ' Hari ' + h + ' Jam.';
  }
  return formattedTime;
  }