function tanggalFormat(tanggalKomentar){
    var d = new Date(tanggalKomentar),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()
  
      var days = new Date(tanggalKomentar),
        month = '' + (days.getMonth() + 1),
        day = '' + days.getDate();
        year = days.getFullYear();
  
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
  
      month = days.toLocaleString('default', { month: 'short' })
      var tanggal = [day, month, year].join(' ')
      dateNew = tanggal
      var formattedTime = hours + ':' + minutes.substr(-2);
      return formattedTime
  }
  
  function hariKomentar(tanggalKomentar){
    var d = new Date(tanggalKomentar),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()
  
      var days = new Date(tanggalKomentar),
        month = '' + (days.getMonth() + 1),
        day = '' + days.getDate();
        year = days.getFullYear();
  
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
      month = days.toLocaleString('default', { month: 'short' })
      var tanggal = [day, month].join(' ')
      dateNew = tanggal
      var formattedTime = hours + ':' + minutes.substr(-2);
      return tanggal
  }
  
  function formatTanggal(tanggalKomentar){
    if(tanggalKomentar==null){
      return ''
    }else{
      var d = new Date(tanggalKomentar),
      hours = (d.getHours()),
      minutes = "0" + d.getMinutes()
  
    var days = new Date(tanggalKomentar),
      month = '' + (days.getMonth() + 1),
      day = '' + days.getDate();
      year = days.getFullYear();
  
      if (month.length < 2)
        month = '0' + month;
      if (day.length < 2)
          day = '0' + day;
    month = days.toLocaleString('default', { month: 'short' })
    var tanggal = [day, month, year].join(' ')
    dateNew = tanggal
    var formattedTime = hours + ':' + minutes.substr(-2);
    var combinedTime = [tanggal,formattedTime].join('-')
    return combinedTime
    }
  }
  
  function konversiHari(t){
    var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor( (t - d * cd) / ch),
        m = Math.round( (t - d * cd - h * ch) / 60000),
        pad = function(n){ return n < 10 ? '0' + n : n; };
  if( m === 60 ){
    h++;
    m = 0;
  }
  if( h === 24 ){
    d++;
    h = 0;
  }
  var formattedTime = d + ' Hari ' + h + ' Jam ' + m + ' menit.';
  return formattedTime;
  }

  function fungsiHari(t){
    var d = new Date(t),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()
  
      var days = new Date(t),
        month = '' + (days.getMonth() + 1),
        day = '' + days.getDate();
        year = days.getFullYear();
  
        if (month.length < 2)
          month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
      month = days.toLocaleString('default', { month: 'short' })
      var tanggal = [day, month, year].join(' ')
      return tanggal
  }

  function dataTables(res,columns){
    $table = $('#example3').find('tbody').empty();
    var table = $('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "aaData": res,
      "columns": columns
    });
    return table
  }

  function updateTable(newData){
    $('#example3').DataTable().clear();
    $('#example3').DataTable().rows.add(newData);
    $('#example3').DataTable().draw(false);
  }
  
  function cariArray(hasil){
    var diff = konversiHari(new Date() - new Date(hasil.crmHistory.tanggalDispose));
    var agingDate={"aging":diff}
    hasil=Object.assign(hasil,agingDate)
    return hasil.crmHistory.mitraById.usernameByMitra.username==usernameMitraReport
  }
  
  function cariArrayNotMitra(hasil){
    var diff = konversiHari(new Date() - new Date(hasil.crmHistory.tanggalDispose));
    var agingDate={"aging":diff}
    hasil=Object.assign(hasil,agingDate)
    return hasil
  }