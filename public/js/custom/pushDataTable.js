var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

   async function dispose(id){
        var crmId = getCookie("idCrm");
        var submitDispose = {
            "crmById": {
                "id": crmId
            },
            "mitraById": {
                "id": id
            },
            "topic": null
        }
        $.ajax({
            type:"POST",
            dataType: "json",
            data:submitDispose,
            url:"/disposeMitrasaveApi",
            success:function(data)
            {
            console.log(data)
            if(data.result == "PA Dengan id ini sudah di dispose"){
                Toast.fire({
                    icon: 'warning',
                    title: ' ' + data.result
                  })
            }else if(data.result == "PA Telah Berhasil di Dispose"){
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  url="/pekerjaan";
                  window.location=url;

            }else{
                Toast.fire({
                    icon: 'error',
                    title: ' ' + data.result
                  })
            }
            $('.btn').prop('disabled', false)
            }
          });
        console.log(submitDispose)
    }

    async function verifikasiAsset(asset){
        const formData = new FormData();
        var disposeId = getCookie("idDispose");
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{"idAsset":asset, "noPa":disposeId, "username":getCookie("username")},
            url:"/verifikasiAssetApi",
            success:function(data)
            {
                console.log(data)
                if(data.result == "Asset sudah di invite pada Pa Ini"){
                    Toast.fire({
                        icon: 'warning',
                        title: ' ' + data.result
                        })
                }else if(data.result == "Asset berhasil di invite"){
                    Toast.fire({
                        icon: 'success',
                        title: ' ' + data.result
                        })
                }else{
                    Toast.fire({
                        icon: 'error',
                        title: ' ' + data.result
                        })
                }
                $('.btn').prop('disabled', false)
            }
            });
    }

    async function sendData(data){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:data,
            url:"/userMitraSaveApi",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  getMitra().then(function(res){
                    updateTable(res)
                  })
                  $('.btn').prop('disabled', false)
            }
          });
    }

    async function sendDataUser(data){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:data,
            url:"/userPenggunaSaveApi",
            success:function(data)
            {
            console.log(data)
            Toast.fire({
                icon: 'success',
                title: ' ' + data.result
              })
              getIcon().then(function(res){
                updateTable(res)
              })
              $('.btn').prop('disabled', false)
            }
          });
    }

    async function sendDataAnggota(data){
        console.log(data)
        $.ajax({
            type:"POST",
            dataType: "json",
            data:data,
            url:"/tambahAnggotaApi",
            success:function(data)
            {
            // console.log(res.data)
            Toast.fire({
                icon: 'success',
                title: ' ' + data.result
              })
              $('.btn').prop('disabled', false)
            }
          });
    }

    async function postKomen(data){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:data,
            url:"/replyApi",
            success:function(data)
            {
                Toast.fire({
                  icon: 'success',
                  title: ' ' + data.result
                })
                timeline();
            }
          });
       
      }

      async function approveSurvei(id, username){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id,
                "username":username
            },
            url:"/approveSurvei",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  $('#submitQC').prop('disabled', false)
            }
          });
    }
    
    async function approveVerifikasi(id, username){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id,
                "username":username
            },
            url:"/approveVerifikasi",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  $('#submitQC').prop('disabled', false)
            }
          });
    }
    
    async function approveLaporan(id, username){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id,
                "username":username
            },
            url:"/approveLaporan",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  $('#submitQC').prop('disabled', false)
            }
          });
    }

    async function approveBaiFot(id){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id,
                "username":getCookie('username')
            },
            url:"/approveBaiFot",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  $('#submitQC').prop('disabled', false)
            }
          });
    }

    async function revisiVerifikasi(id, username){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id,
                "username":username
            },
            url:"/revisiVerifikasi",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
                  $('#submitQC').prop('disabled', false)
            }
          });
    }

    function changePassword(datas){
        console.log(datas)
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "username":datas.username,
                "oldPassword":datas.oldPassword,
                "newPassword":datas.newPassword,
            },
            url:"/changeUserPassword",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result.message
                  })
            },
            statusCode: {
                500: function() {
                    Toast.fire({
                        icon: 'error',
                        title: ' ' + "Invalid Password"
                      })
                }
            }
          });
    }

    async function revisiBaiFot(id, username){
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id,
                "username":username
            },
            url:"/revisiBaiFot",
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
            }
          });
    }

    async function terimaInvite(id){
        console.log(id)
        $.ajax({
            type:"POST",
            dataType: "json",
            data:{
                "id":id
            },
            url:"/acceptInvite/"+id,
            success:function(data)
            {
                console.log(data)
                Toast.fire({
                    icon: 'success',
                    title: ' ' + data.result
                  })
            }
          });
    }