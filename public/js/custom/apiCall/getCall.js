async function getCrm(){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/crmApi/"
    })
}

async function getHistoryList(){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/HistorylistApi/"
    })
}

async function getMitra(){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/mitraApi/"
    })
}

async function getIcon(){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/iconApi/"
    })
}

async function getAsset(){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/assetApi"
    })
}

async function getAttachProgress(idCrm, idDetail){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/attachProgressApi/"+idCrm+"/"+idDetail
    })
}

async function getCrmId(idCrm){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/crmIdApi/"+idCrm
    })
}

async function getMitraId(idMitra){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/mitraApiId/"+idMitra
    })
}

async function getReply(idPekerjaan){
    return $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/getReplyApi/"+idPekerjaan
    })
}