
$(function () {
  function showKomentar(value){
    var html =''
    console.log(value)
    $.each(value,function(index,value){
      html += '<div style="margin-left:20px"><p class="timeline-header"><a href="#">'+value.userByReply.username+'</a><span style="color: #999;font-size: 12px;float:right" class="time"><i class="fas fa-clock"></i> '+hariKomentar(value.tanggalReply)+', '+tanggalFormat(value.tanggalReply)+'</span><div>'+value.catatan+'</div></div><hr>'
    })
    return html
  }

  function showKomen(value){
    getReply(value.id).then(data => showKomentar(data))
  }

  function cekPekerjaan(){
      $('#pekerjaan').append(
        $('<td colspan="6">').append(
          $('<center>').append(
            $('<p>').text("Mohon Tunggu, Data sedang Dipanggil")
          )
        )
      )
      getCrm().then(function(res){
        var column = [
          {data:"noPA"},
          {data:"customerName"},
          {data:"productName"},
          {data:"address"},
          {data:"projectTeamLeader"},
          {
            "defaultContent": "<button class='btn btn-block btn-primary'>Dispose</button>"
          }
        ]
        var table = dataTables(res,column)
        $('#example3 tbody').on( 'click', 'button', function () {
          var davitas
          var datas = table.row( $(this).parents('tr') ).data();
          var vitas = table.row( $(this)).data();
          if(typeof datas=='undefined'){
            davitas=vitas;
          }else if(typeof vitas=='undefined'){
            davitas=datas;
          }
          document.cookie = "idCrm=" + davitas.id;
          document.cookie = "dispose=mitra"
          url="/dispose";
          window.location=url;
      });
      })
}

 function cekReport(){
  var usernameMitraReport = getCookie("username")
  var indexArray
  var aging = 0
  getHistoryList().then(function(res){
    var row = getCookie('cookieRoles')==='j:["ROLE_MITRA"]'? res.filter(cariArray):res.filter(cariArrayNotMitra)
    var column = [
      {data:"crmHistory.crmById.noPA"},
      {data:"crmHistory.crmById.owner"},
      {data:"crmHistory.crmById.address"},
      {data:"crmHistory.mitraById.namaMitra"},
      {data:"status"},
      {data:"aging"},
      {
        "defaultContent": "<button class='btn btn-block btn-primary'>Detail</button>"
      }
    ]
    var table = dataTables(row, column)
      if(historyLOG==1){
      $('#example3 tbody').on( 'click', 'button', function () {
        var davitas
          var datas = table.row( $(this).parents('tr') ).data();
          var vitas = table.row( $(this)).data();
          if(typeof datas=='undefined'){
            davitas=vitas;
          }else if(typeof vitas=='undefined'){
            davitas=datas;
          }
        document.cookie = "idHistory=" + davitas.id;
        document.cookie = "idCrm=" + davitas.crmHistory.crmById.id;
        document.cookie = "idDispose=" + davitas.crmHistory.id;
        document.cookie = "idMitra=" + davitas.crmHistory.mitraById.id
        console.log(davitas)
        url="/detailhistory";
        window.location=url;
        console.log(data.crmHistory.id)
    }); 
      }else{
        $('#example3 tbody').on( 'click', 'button', function () {
          var davitas
          var datas = table.row( $(this).parents('tr') ).data();
          var vitas = table.row( $(this)).data();
          if(typeof datas=='undefined'){
            davitas=vitas;
          }else if(typeof vitas=='undefined'){
            davitas=datas;
          }
          document.cookie = "idCrm=" + davitas.crmHistory.crmById.id;
          document.cookie = "idDispose=" + davitas.crmHistory.id;
          document.cookie = "idMitra=" + davitas.crmHistory.mitraById.id
          url="/uploadpekerjaan";
          window.location=url;
          console.log(davitas.crmHistory.id)
      });
      }
  })
  $('#reportPA').append(
    $('<td colspan="6">').append(
      $('<center>').append(
        $('<p>').text("Mohon Tunggu, Data sedang Dipanggil")
      )
    )
  )
  // return res;
}
function cekMitra(){
  $('#cekMitra').append(
    $('<td colspan="6">').append(
      $('<center>').append(
        $('<p>').text("Mohon Tunggu, Data sedang Dipanggil")
      )
    )
  )
  showMitraTable()
  // getMitra().then(function(res){
  //   var column = [
  //     {data:"namaMitra"},
  //     {data:"alamat"},
  //     {data:"pic"},
  //     {data:"noHP"},
  //     {data:"email"},
  //     {
  //         "defaultContent": "<button id='disposeTombol' class='btn btn-block btn-primary'>Lihat</button>"
  //     }
  //   ]

  //   var table = dataTables(res, column)
  //   $('#example3 tbody').on( 'click', 'button', function () { 
  //     var davitas
  //     var datas = table.row( $(this).parents('tr') ).data();
  //     var vitas = table.row( $(this)).data();
  //     if(typeof datas=='undefined'){
  //       davitas=vitas;
  //     }else if(typeof vitas=='undefined'){
  //       davitas=datas;
  //     }
  //     document.cookie = "idMitra=" + davitas.id;
  //     url="/detailmitra";
  //     window.location=url;
  //     console.log(davitas)
  //   });
  // })
}

function cekUser(){
  showUserTable()
  $('#cekUser').append(
    $('<td colspan="6">').append(
      $('<center>').append(
        $('<p>').text("Mohon Tunggu, Data sedang Dipanggil")
      )
    )
  )
    console.log(Link.icon)
}
function disposeMitra(){
  $('#example3').find('tbody').append(
    $('<td colspan="6">').append(
      $('<center>').append(
        $('<p>').text("Mohon Tunggu, Data sedang Dipanggil")
      )
    )
  )
  getMitra().then(function(res){
    var column = [
      {data:"namaMitra"},
      {data:"alamat"},
      {data:"pic"},
      {data:"noHP"},
      {data:"email"},
      {
          "defaultContent": "<button id='disposeTombol' class='btn btn-block btn-primary'>Submit</button>"
      }
    ]
    var table = dataTables(res,column)
    $('#example3 tbody').on( 'click', 'button', function () {
      $('.btn').prop('disabled', true)
      var davitas
      var datas = table.row( $(this).parents('tr') ).data();
      var vitas = table.row( $(this)).data();
      if(typeof datas=='undefined'){
        davitas=vitas;
      }else if(typeof vitas=='undefined'){
        davitas=datas;
      }
        dispose(davitas.id)
        console.log(data)
    });
  })
}

async function disposeAsset(){
  getIcon().then(function(res){
    var column = [
      {data:"namaLengkap"},
      {data:"wilayahKerja"},
      {data:"noHP"},
      {data:"email"},
      {
          "defaultContent": "<button id='disposeButton' class='btn btn-block btn-primary'>Submit</button>"
      }
    ]
    var table = dataTables(res,column)
      $('#example3 tbody').on( 'click', 'button', function () {
        $('.btn').prop('disabled', true)
        var davitas
        var datas = table.row( $(this).parents('tr') ).data();
        var vitas = table.row( $(this)).data();
        if(typeof datas=='undefined'){
          davitas=vitas;
        }else if(typeof vitas=='undefined'){
          davitas=datas;
        }          
        verifikasiAsset(davitas.id)
          console.log(davitas)
      });
  })
  $('#example3').find('tbody').append(
    $('<td colspan="6">').append(
      $('<center>').append(
        $('<p>').text("Mohon Tunggu, Data sedang Dipanggil")
      )
    )
  )
}

(function ($) {
  $.fn.serializeFormJSON = function () {

      var o = {};
      var a = this.serializeArray();
      $.each(a, function () {
          if (o[this.name]) {
              if (!o[this.name].push) {
                  o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
          } else {
              o[this.name] = this.value || '';
          }
      });
      return o;
  };
})(jQuery);

$(document).on('submit', 'form', function(e){
  e.preventDefault();
  var data = $(this).serializeFormJSON();
  var datas = {
    "catatan":data.komento,
    "userByReply":{
      "username":getCookie("username")
    },
    "replyPekerjaan":{
      "id":data.idProgress
    }
  }
  console.log(data)
  postKomen(datas)
})


function timeline(){
  var idCrm = getCookie("idDispose")
  var idDetail = getCookie("idDetail")
  var dateOld;
  var dateNew;
  
  getAttachProgress(idCrm, idDetail).then(function(res){
    $.each(res, function( index, value ){
      var fotoSplit = value.dokumen.split(",");
      var jumlahFoto = fotoSplit.length
      var fotoSatu
      var fotoMulti=[]
      if(jumlahFoto>1){
        $.each(fotoSplit,function(index,value){
          fotoMulti.push(value)
        })
      }else{
        fotoSatu=fotoSplit[0]
      }
      var tanggal = fungsiHari(value.tanggalUpload)
      var d = new Date(value.tanggalUpload),
        hours = (d.getHours()),
        minutes = "0" + d.getMinutes()

      dateNew = tanggal
      var formattedTime = hours + ':' + minutes.substr(-2);
      $('#timeline').append((dateOld!=dateNew ?
        $('<div class="time-label">').append(
          $('<span class="bg-red">').text(tanggal),
        ): ''),
        
        $('<div>').append((value.tipeDokumen=="Foto Dokumen"? 
        $('<i class="fa fa-camera bg-purple">'): 
        $('<i class="fas fa-envelope bg-blue">')),
          $('<div class="timeline-item">').append(
            $('<span class="time"><i class="fas fa-clock"></i> '+formattedTime+'</span>'),
            $('<h3 class="timeline-header"><a href="#">'+value.userByProgress.username+'</a> Mengunggah '+value.tipeDokumen+'</h3>'),
          $('<div class="timeline-body">').append((value.tipeDokumen=="Foto Dokumen" ? 
            $('<div>').append(function(){
              var html = ''
              if(jumlahFoto>1){
                $.each(fotoMulti,function(index,value){
                  html+='<a href="'+Link.downloadFile+value+'" data-toggle="lightbox" data-title="'+value+'"><img src="'+Link.downloadFile+value+'" style="width:20%" class="img-fluid mb-2" alt="">'
                })
              }else{
                html+='<a href="'+Link.downloadFile+fotoSatu+'" data-toggle="lightbox" data-title="'+fotoSatu+'"><img src="'+Link.downloadFile+fotoSatu+'" style="width:20%" class="img-fluid mb-2" alt="">'
              }
              return html
            },
              $('<div>').text(value.catatan)
            ):
            $('<div>').append(
              $('<embed src="'+Link.downloadFile+encodeURIComponent(value.dokumen.trim())+'#scrollbar=0" width="100px" height="129px" />'),
              //DI SERVER BELOM ADA(ONORE TARJON) $('<iframe src="https://docs.google.com/gview?url=http://139.59.47.25:98/uploadFile/downloadFile/"'+value.dokumen+'"&embedded=true"></iframe>'),
            $('<br>'),
            $('<div>').text(value.catatan)
            )
          )
          ),
          $('<div class="timeline-footer">').append((value.tipeDokumen=="Foto Dokumen")?
              $('<span></span>'):
              $('<div>').append(
                $('<a  id="lihatDokumen" class="btn btn-primary btn-sm" href="'+Link.downloadFile+value.dokumen+'#toolbar=0" data-toggle="lightbox" data-title="'+value.dokumen+'">Lihat</a>'),
                $('<span>   </span>'),
                $('<a id="downloadDokumen" class="btn btn-primary btn-sm">Download</a>').on('click', function(e){downloadFile(value.dokumen); })
              )
            ),
            $('<div class="timeline-footer" style="background-color:whitesmoke">').append(
            showKomentar(value.replyByProgress),
            // showKomen(value),
            $('<form id="test">').append(
              $('<div class="user-panel mt-3 pb-3 mb-3 d-flex">').append(
                $('<div class="image">').append(
                  $('<img src="'+Link.profilePicture+getCookie("cookiePicture")+'" class="img-circle elevation-2" alt="User Image">')
                ),
                $('<input name="komento" style="margin-left:10px;margin-right:10px" class="form-control form-control-sm" type="text" placeholder="Masukkan Komentar">'),
                $('<input name="idProgress" value='+value.id+' hidden>'),
                $('<button type="submit" class="btn btn-primary btn-sm">Komentar</button>')
              )
            )
            )
          )
        )
      )
      dateOld=dateNew
    })
  })
}

async function timelineHistory(){
var idCrm = getCookie("idHistory")
var idDetail = getCookie("idDetail")
var dateOld;
var dateNew;
// console.log(Link.historyListDetail+idCrm)
const res = await axios.get(Link.historyListDetail+idCrm)
.then(res=>{
  $.each(res.data, function( index, value ){
    console.log("histori",value)
    var d = new Date(value.created_at),
      hours = (d.getHours()),
      minutes = "0" + d.getMinutes()

      var tanggal = fungsiHari(value.created_at)
    // var days = new Date(value.created_at),
    //   month = '' + (days.getMonth() + 1),
    //   day = '' + days.getDate();
    //   year = days.getFullYear();

    //   if (month.length < 2)
    //     month = '0' + month;
    //   if (day.length < 2)
    //       day = '0' + day;
    // month = days.toLocaleString('default', { month: 'short' })
    // var tanggal = [day, month, year].join(' ')
    dateNew = tanggal
    var formattedTime = hours + ':' + minutes.substr(-2);
    $('#timeline').append((dateOld!=dateNew ?
      $('<div class="time-label">').append(
        $('<span class="bg-red">').text(tanggal),
      ): ''),
      
      $('<div>').append(function(){
        var html = ''
        switch (value.type) {
          case 1:
            html = '<i class="fas bg-orange">'
            break;
          case 4:
            html = '<i class="fas bg-green">'
            break;
          case 2:
            html = '<i class="fas bg-blue">'
            break;
          case 5:
            html = '<i class="fas bg-red">'
            break;
          // case 1:
          //   html = '<i class="fas fa-photo-video bg-orange">'
          //   break;
          // case 'Input Laporan':
          //   html = '<i class="far fa-file-alt bg-green">'
          //   break;
          // case 'Input Verifikasi':
          //   html = '<i class="fas fa-check bg-blue">'
          //   break;
          case 3:
            html = '<i class="fas fa-bolt bg-red">'
            break;
          case 0:
            html = '<i class="fas fa-home bg-yellow">'
            break;
          // case 'PA Telah Di Dispose':
          //   html = '<i class="fas fa-dollar-sign bg-cyan">'
          //   break;
          default:
            html = '<i class="fas bg-transparent">'
            break;
        }
        return html
      },
        $('<div class="timeline-item">').append(
          $('<span class="time"><i class="fas fa-clock"></i> '+formattedTime+'</span>'),
        $('<div class="timeline-body">').append((value.catatan=="GDB" ? 
          $('<div>').append(
            $('<div>').text(value.catatan)
          ):
          $('<div>').text(value.catatan)
        )
        ),
        $('<div class="timeline-footer">').append(function(){
          var html = ''
          switch (value.type) {
            case 1:
              var detiles = 1
              html = '<a class="btn btn-primary btn-sm" onClick="periksaSurvei('+detiles+')">Lihat Survei</a>'
              break;
            case 3:
              var detiles = 3
              html = '<a class="btn btn-primary btn-sm" onClick="periksaSurvei('+detiles+')">Lihat Bai Fot</a>'
              break;
            case 2:
              var detiles = 2
              html = '<a class="btn btn-primary btn-sm" onClick="periksaSurvei('+detiles+')">Lihat Verifikasi</a>'
              break;
            case 4:
              var detiles = 4
              html = '<a class="btn btn-primary btn-sm" onClick="periksaSurvei('+detiles+')">Lihat Laporan</a>'
              break;
            default:
              var detiles = 5
              html = '<a class="btn btn-primary btn-sm" onClick="periksaSurvei('+detiles+')">Lihat</a>'
              break;
          }
          return html
          // $('<a class="btn btn-primary btn-sm">Lihat Pekerjaan</a>').on('click', function(e){downloadFile(value.dokumen); })
        }
          )
        )
      )
    )
    dateOld=dateNew
  })
})
}

async function statusPA(){
  var detailStatusPekerjaan = getCookie("idDetail")
  if(detailStatusPekerjaan=="Survei"){
    var res = await axios.get(Link.detailSurvei+getCookie("idDispose"))
    .then(res=>{
      var column = [
        {data:"namaJalan"},
        {data:"kodeTiang"},
        {data:"jarakAntarTiang"},
        {data:"panjangKabel"},
        {data:"koordinatGps"},
        {data:"statusTiang"},
        {data:"srPln"},
        {data:"srIcon"},
        {data:"srNonicon"},
        {data:"tipeTiang"},
        {data:"fittingSuspension"},
        {data:"fittingDeadend"},
        {data:"label"},
        {data:"galian"},
        {data:"odf"},
        {data:"otb"},
        {data:"existingJb"},
        {data:"newJb"},
        {data:"patchCore"},
        {data:"opticalTermination"},
        {data:"opticalJointing"},
        {data:"otdrTest"},
        {data:"keterangan"}
      ]
      var table = dataTables(res.data.bomById,column)
      $('#lihatFotoDetail').empty()
      $.each(res.data.surveiByFoto,function(index,value){
        $('#lihatFotoDetail').append(
          $('<div class="col-sm-2">').append(
            $('<a href="'+Link.downloadFile+value.fileName+'" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">').append(
              $('<img src="'+Link.downloadFile+value.fileName+'" class="img-fluid mb-2" alt="white sample"/>')
            )
          )
        )
      })
      $('#sharingan').hide()
      document.cookie="idPekerjaan="+res.data.id
      $('#statusDetailPekerjaan').empty().append(
        $('<thead>').append(
          $('<tr>').append(
            $('<th>').text('Form BOM'),
            $('<th>').text('GDB BOM'),
            $('<th>').text('Dokumentasi'),
            $('<th>').text('Status'),
            $('<th>').text('Tanggal Upload'),
            $('<th>').text('Tanggal Update')
          )
        ),
        $('<tbody>').append(
          $('<tr>').append(
            $('<td>').append(
              $('<center>').append(
              $('<a style="font-size:10pt" data-toggle="modal" data-target="#modal-overlayy" id="lihatBom" class="btn btn-primary btn-xs">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspLihat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>')
              )
            ),
            $('<td>').text(res.data.gdb),
            $('<td>').append(
              $('<center>').append(
              $('<a style="font-size:10pt" data-toggle="modal" data-target="#modal-overlay" id="lihatDokumen" class="btn btn-primary btn-xs">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspLihat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>')
              )
            ),
              // $('<td>').text(res.data.surveiByFoto),
            $('<td>').append(
              function(){
                var html=''
                switch (res.data.status) {
                  case 1:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                
                  case 2:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                  
                  case 3:
                    html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
                    $('#submitQC').hide()
                    break;
                  
                  case 4:
                    html = '<span class="badge bg-error">Memerlukan revisi</span>'
                    break;
                  default:
                    break;
                }
                return html
              },
            ),
            $('<td>').text(formatTanggal(res.data.tanggalUploadBom)),
            $('<td>').text(formatTanggal(res.data.tanggalUpdateBom))
          )
        )
      )
      console.log(res)
    })
  }else if(detailStatusPekerjaan=="Verifikasi"){
    var res2 = await axios.get(Link.detailVerifikasi+getCookie("idDispose"))
    .then(res=>{
      document.cookie="idPekerjaan="+res.data.id
      $('#statusDetailPekerjaan').empty().append(
        $('<thead>').append(
          $('<tr>').append(
            $('<th>').text('Data Core Verifikasi'),
            $('<th>').text('GDB Verifikasi'),
            $('<th>').text('Catatan'),
            $('<th>').text('Status'),
            $('<th>').text('Tanggal Upload'),
            $('<th>').text('Tanggal Update')
          )
        ),
        $('<tbody>').append(
          $('<tr>').append(
            $('<td>').text(res.data.dataCore),
            $('<td>').text(res.data.gdb),
            $('<td>').text(res.data.catatanVerifikasi),
            $('<td>').append(
              function(){
                var html=''
                switch (res.data.status) {
                  case 1:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                
                  case 2:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                  
                  case 3:
                    html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
                    $('#submitQC').hide()
                    break;
                  
                  case 4:
                    html = '<span class="badge bg-error">Memerlukan revisi</span>'
                    break;
                  default:
                    break;
                }
                return html
              },
            ),
            $('<td>').text(formatTanggal(res.data.tanggalUploadVerifikasi)),
            $('<td>').text(formatTanggal(res.data.tanggalUpdateVerifikasi))
          )
        )
      )
      console.log(res)
    })
  }else if(detailStatusPekerjaan=="Laporan Aktivasi"){
    var res3 = await axios.get(Link.detailLaporan+getCookie("idDispose"))
    .then(res=>{
      document.cookie="idPekerjaan="+res.data.id
      $('#statusDetailPekerjaan').empty().append(
        $('<thead>').append(
          $('<tr>').append(
            $('<th>').text('Form BOM Laporan'),
            $('<th>').text('Form BOQ Laporan'),
            $('<th>').text('Laporan Aktivasi'),
            $('<th>').text('Data Core Laporan'),
            $('<th>').text('Status'),
            $('<th>').text('Tanggal Upload'),
            $('<th>').text('Tanggal Update')
          )
        ),
        $('<tbody>').append(
          $('<tr>').append(
            $('<td>').text(res.data.bom),
            $('<td>').text(res.data.boq),
            $('<td>').text(res.data.laporan1),
            $('<td>').text(res.data.dataCore),
            $('<td>').append(
              function(){
                var html=''
                switch (res.data.status) {
                  case 1:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                
                  case 2:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                  
                  case 3:
                    html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
                    $('#submitQC').hide()
                    break;
                  
                  case 4:
                    html = '<span class="badge bg-error">Memerlukan revisi</span>'
                    break;
                  default:
                    break;
                }
                return html
              },
            ),
            $('<td>').text(formatTanggal(res.data.tanggalUploadLaporan)),
            $('<td>').text(formatTanggal(res.data.tanggalUpdateLaporan))
          )
        )
      )
      console.log(res)
    })
  }else if(detailStatusPekerjaan=="Bai Fot"){
    var res3 = await axios.get(Link.detailBaifot+getCookie("idDispose"))
    .then(res=>{
      document.cookie="idPekerjaan="+res.data.id_baiTestcom
      console.log(res)
      $('#statusDetailPekerjaan').empty().append(
        $('<thead>').append(
          $('<tr>').append(
            $('<th>').text('File Testcom'),
            $('<th>').text('File BAI'),
            $('<th>').text('Status'),
            $('<th>').text('Tanggal Upload'),
            $('<th>').text('Tanggal Update')
          )
        ),
        $('<tbody>').append(
          $('<tr>').append(
            $('<td>').text(res.data.testCom),
            $('<td>').text(res.data.bai),
            $('<td>').append(
              function(){
                var html=''
                switch (res.data.status) {
                  case 1:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                
                  case 2:
                    html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                    break;
                  
                  case 3:
                    html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
                    $('#submitQC').hide()
                    break;
                  
                  case 4:
                    html = '<span class="badge bg-error">Memerlukan revisi</span>'
                    break;
                  default:
                    break;
                }
                return html
              },
            ),
            $('<td>').text(formatTanggal(res.data.tanggalUpload)),
            $('<td>').text(formatTanggal(res.data.tanggalUpdate))
          )
        )
      )
      console.log(res)
    })
  }
}

async function detailPA(){
  var idCrm = getCookie("idCrm")
    getCrmId(idCrm).then(function(res){
      $("#paNode").text(res.noPA)
      $('#customerNode').text(res.customerName)
      $("#service").text(res.productName)
      $('#bandwidth').text(res.bandwidth)
      $("#ioNumber").text(res.ionumber)
      $('#address').text(res.address)
      $("#ptl").text(res.projectTeamLeader)
    })
}

async function statusKerja(){
  var idStatus = getCookie("idDispose")
  const res = await axios.get(Link.statusPekerjaan+idStatus)
  .then(res=>{
    console.log(res.data)
    if(res.data.StatusSurvei=="Belum Mengupload Survei"){
      $('agingSurvei').text('')
      $('#uploadDokumenButton2').prop("disabled",true)
      $('#uploadDokumenButton3').prop("disabled",true)
      $('#uploadDokumenButton4').prop("disabled",true)
      
    }else{
      $('#agingSurvei').text(konversiHari(res.data.SurveiAging))
    }
    if(res.data.StatusVerifikasi=="Belum Mengupload Verifikasi"){
      $('agingVerifikasi').text('')
      $('#uploadDokumenButton2').prop("disabled",true)
      $('#uploadDokumenButton3').prop("disabled",true)
      $('#uploadDokumenButton4').prop("disabled",true)
    }else{
    $('#agingVerifikasi').text(konversiHari(res.data.VerifikasiAging))
    }
    if(res.data.StatusLaporan=="Belum Menginput Laporan"){
      $('agingLaporan').text('')
      $('#uploadDokumenButton3').prop("disabled",true)
    }else{
      $('#agingLaporan').text(konversiHari(res.data.LaporanAging))
    }if(res.data.StatusBaiFot=="Belum Mengupload Bai Fot"){
      $('agingBaifot').text('')
      $('#uploadDokumenButton3').prop("disabled",true)
      $('#uploadDokumenButton4').prop("disabled",true)
    }else{
      $('#agingBaifot').text(konversiHari(res.data.BaiFotAging))
    }
    if(res.data.StatusAsset == "Belum Mengsubmit Asset"){
      $('#disposeAsset').text(res.data.StatusAsset)
      $('#assetStatusNotif').hide()
    }else{
      $('#disposeAsset').text(res.data.StatusAsset).hide()
      $('#uploadDokumenButton2').prop("disabled",false)
    }
    $('#statusBOM').append(
      function(){
        var html=''
        switch (res.data.StatusSurvei) {
          case 1:
            html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
            break;
        
          case 2:
            html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
            break;
          
          case 3:
            html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
            $('#uploadDokumenButton').prop("disabled",true)
            break;
          
          case 4:
            html = '<span class="badge bg-error">Memerlukan revisi</span>'
            break;
          default:
            html = '<span class="badge bg-info">Belum ada dokumen</span>'
            break;
        }
        return html
      }),
    $('#statusVerifikasi').append(
      function(){
      var html=''
      switch (res.data.StatusVerifikasi) {
        case 1:
          html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
          break;
      
        case 2:
          html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
          break;
        
        case 3:
          html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
          $('#uploadDokumenButton2').prop("disabled",true)
          if(getCookie("cookieRoles")=='j:["ROLE_ASSET"]'){
            $('#uploadDokumenButton4').text("-").prop("disabled",true)
          }else{
            $('#uploadDokumenButton4').prop("disabled",false)
          }
          break;
        
        case 4:
          html = '<span class="badge bg-error">Memerlukan revisi</span>'
          break;
        default:
          html = '<span class="badge bg-info">Belum ada dokumen</span>'
          break;
      }
      return html
    }),
      $('#statusBaifot').append(
        function(){
          var html=''
          switch (res.data.StatusBaiFot) {
            case 1:
              html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
              break;
          
            case 2:
              html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
              break;
            
            case 3:
              html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
              $('#uploadDokumenButton4').prop("disabled",true)
              $('#uploadDokumenButton3').prop("disabled",false)
              break;
            
            case 4:
              html = '<span class="badge bg-error">Memerlukan revisi</span>'
              break;
            default:
              html = '<span class="badge bg-info">Belum ada dokumen</span>'
              break;
          }
          return html
        }),
        $('#statusLaporan').append(
          function(){
            var html=''
            switch (res.data.StatusLaporan) {
              case 1:
                html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                break;
            
              case 2:
                html = '<span class="badge bg-warning">Menunggu verifikasi</span>'
                break;
              
              case 3:
                html = '<span class="badge bg-primary">Sudah diverifikasi</span>'
                $('#uploadDokumenButton3').prop("disabled",true)
                break;
              
              case 4:
                html = '<span class="badge bg-error">Memerlukan revisi</span>'
                break;
              default:
                html = '<span class="badge bg-info">Belum ada dokumen</span>'
                break;
            }
            return html
          })
  })
}

async function detailMitra(){
  var idMitra = getCookie("idMitra")
  getMitraId(idMitra).then(function(res){
    $("#namaMitra").text(res.namaMitra)
    $('#alamatMitra').text(res.alamat)
    $('#picMitra').text(res.pic)
    $('#kontakMitra').text(res.noHP)
    $("#emailMitra").text(res.email)
    var column = [
      {data:"namaPerson"},
      {data:"noHp"},
      {data:"email"},
      {
        "defaultContent": "<button type='button' class='btn btn-default'>Action</button>"
      }
    ]
    dataTables(res.personData,column)
  })
}
async function downloadFile(dokumen){
  window.open(Link.downloadFile+dokumen, '_blank');
  return false;
//   $.ajax({
//   type:"GET",
//   dataType: "json",
//   data:{name: "name"},
//   url:"/downloadFileApi/"+dokumen,
//   success:function(res)
//   {
//     console.log(res)
//   }
// });

}

if(tables == "table0"){
  detailPA()
  disposeAsset()
}else if(tables == "table1"){
  cekPekerjaan()
}else if(tables == "table2"){
  cekReport()
}else if(tables == "table3"){
  cekMitra()
}else if(tables == "table4"){
  cekUser()
}else if(tables == "table5"){
  disposeMitra()
  detailPA();
}else if(tables == "table6"){
  detailPA();
  statusKerja();
}else if(tables == "table7"){
  detailPA();
  statusPA();
  timeline();
}else if(tables == "table8"){
  timelineHistory();
}else if(tables == "table9"){
  detailMitra();
}
});

function periksaSurvei(value){
  switch (value) {
  case 1:
      document.cookie = "idDetail=" + "Survei";
      break;
  case 2:
      document.cookie = "idDetail=" + "Verifikasi";
      break;
  case 3:
      document.cookie = "idDetail=" + "Bai Fot";
      break;
  case 4:
      document.cookie = "idDetail=" + "Laporan Aktivasi";
      break;
    default:
      break;
  }
  url="/detailPekerjaan";
  window.location=url;
}