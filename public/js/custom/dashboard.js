kotakDashboard();
async function kotakDashboard(){
    $.ajax({
        type:"GET",
        dataType: "json",
        data:{name: "name"},
        url:"/dashboardApi",
        success:function(res)
        {
            document.getElementById("meterPA").style.width = res.jumlahPA+"%";
            document.getElementById("meterProgress").style.width = res.verifikasionProgress+"%";
            document.getElementById("meterClosed").style.width = res.verifikasiClosed+"%";
            $('#grafikDashboard').text(res.ShowMonth),
            $('#kotak1').text(res.jumlahPA),
            $('#kotak2').text(res.verifikasionProgress),
            $('#kotak3').text(res.verifikasiClosed),
            $('#kotak4').text(res.Averageverifikasil),
            $('#1kotak').text(res.jumlahPA),
            $('#2kotak').text(res.verifikasionProgress),
            $('#3kotak').text(res.verifikasiClosed),
            $('#4kotak').text(res.Averageverifikasil)
        }
    });
}